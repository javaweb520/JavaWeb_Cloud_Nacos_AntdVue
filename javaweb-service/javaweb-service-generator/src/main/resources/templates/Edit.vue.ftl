<!-- 编辑弹窗 -->
<template>
  <a-modal
    :width="460"
    :visible="visible"
    :confirm-loading="loading"
    :title="isUpdate?'修改${tableAnnotation}':'添加${tableAnnotation}'"
    :body-style="{paddingBottom: '8px'}"
    @update:visible="updateVisible"
    @ok="save">
    <a-form
      ref="form"
      :model="form"
      :rules="rules"
      :label-col="{md: {span: 5}, sm: {span: 24}}"
      :wrapper-col="{md: {span: 19}, sm: {span: 24}}">
<#if model_column?exists>
  <#list model_column as model>
    <#if model.changeColumnName?uncap_first != "createUser" && model.changeColumnName?uncap_first != "createTime" && model.changeColumnName?uncap_first != "updateUser" && model.changeColumnName?uncap_first != "updateTime" && model.changeColumnName?uncap_first != "mark">
      <#if (model.columnType = 'VARCHAR' || model.columnType = 'CHAR' || model.columnType = 'TEXT' || model.columnType = 'MEDIUMTEXT')>
      <#if model.columnImage == true>
      <a-form-item
        label="${model.columnComment}:"
        :label-col="{sm: {span: 5}, xs: {span: 6}}"
        :wrapper-col="{sm: {span: 21}, xs: {span: 18}}">
        <uploadImage :limit="1" :updDir="updDir" v-model:value="form.${model.columnName}"/>
      </a-form-item>
      <#elseif model.columnTextArea == true>
      <a-form-item label="${model.columnComment}:">
        <a-textarea
          v-model:value="form.${model.columnName}"
          placeholder="请输入${model.columnComment}"
          :rows="4"/>
      </a-form-item>
      <#else>
      <a-form-item label="${model.columnComment}:" name="${model.columnName}">
        <a-input
          v-model:value="form.${model.columnName}"
          placeholder="请输入${model.columnComment}"
          allow-clear/>
      </a-form-item>
      </#if>
      </#if>
      <#if (model.columnType = 'DATETIME' || model.columnType = 'DATE' || model.columnType = 'TIME' || model.columnType = 'YEAR' || model.columnType = 'TIMESTAMP') >
      <a-form-item
        label="${model.columnComment}:"
        name="${model.columnName}">
        <a-date-picker
          format="YYYY-MM-DD HH:mm:ss"
          show-time
          :disabled-date="disabledStartDate"
          v-model:value="form.${model.columnName}"
          value-format="YYYY-MM-DD HH:mm:ss"
          placeholder="请选择${model.columnComment}"
          class="ele-fluid"/>
      </a-form-item>
      </#if>
      <#if (model.columnType = 'TINYINT UNSIGNED' || model.columnType = 'TINYINT')>
        <#if model.columnSwitch == true>
      <a-form-item label="${model.columnCommentName}" name="${model.columnName}">
        <a-radio-group
          v-model:value="form.${model.columnName}">
          <#if model.columnCommentValue?exists>
            <#list model.columnCommentValue?keys as key>
          <a-radio :value="${key}">${model.columnCommentValue[key]}</a-radio>
            </#list>
          </#if>
        </a-radio-group>
      </a-form-item>
        <#else>
      <a-form-item
        label="${model.columnCommentName}:"
        name="${model.columnName}">
        <a-select
          v-model:value="form.${model.columnName}"
          placeholder="请选择${model.columnCommentName}"
          allow-clear>
          <#if model.columnCommentValue?exists>
            <#list model.columnCommentValue?keys as key>
          <a-select-option :value="${key}">${model.columnCommentValue[key]}</a-select-option>
            </#list>
          </#if>
        </a-select>
      </a-form-item>
        </#if>
      </#if>
      <#if (model.columnType = 'INT UNSIGNED' || model.columnType = 'INT' || model.columnType = 'SMALLINT UNSIGNED' || model.columnType = 'SMALLINT' || model.columnType = 'BIGINT UNSIGNED' || model.columnType = 'BIGINT' || model.columnType = 'MEDIUMINT UNSIGNED' || model.columnType = 'MEDIUMINT')>
      <#if model.hasColumnCommentValue = true>
      <a-form-item
        label="${model.columnCommentName}："
        name="${model.columnName}">
        <a-select
          v-model:value="form.${model.columnName}"
          placeholder="请选择${model.columnCommentName}"
          allow-clear>
          <#if model.columnCommentValue?exists>
            <#list model.columnCommentValue?keys as key>
          <a-select-option :value="${key}">${model.columnCommentValue[key]}</a-select-option>
            </#list>
          </#if>
        </a-select>
      </a-form-item>
      <#else>
      <a-form-item label="${model.columnComment}:" name="${model.columnName}">
        <a-input-number
          :min="0"
          class="ele-fluid"
          placeholder="请输入${model.columnComment}"
          v-model:value="form.${model.columnName}"/>
      </a-form-item>
      </#if>
      </#if>
    </#if>
  </#list>
</#if>
    </a-form>
  </a-modal>
</template>

<script>
<#if model_column?exists>
  <#list model_column as model>
  <#if model.columnImage == true>
  import uploadImage from '@/components/uploadImage'

  </#if>
  </#list>
</#if>
  export default {
    name: '${entityName}Edit',
    emits: [
      'done',
      'update:visible'
    ],
    props: {
      // 弹窗是否打开
      visible: Boolean,
      // 修改回显的数据
      data: Object
    },
<#if model_column?exists>
  <#list model_column as model>
    <#if model.columnImage == true>
    components: {uploadImage},
    </#if>
  </#list>
</#if>
    data() {
      return {
        // 表单数据
        form: Object.assign({}, this.data),
        // 表单验证规则
        rules: {
<#if model_column?exists>
  <#list model_column as model>
    <#if model.changeColumnName?uncap_first != "createUser" && model.changeColumnName?uncap_first != "createTime" && model.changeColumnName?uncap_first != "updateUser" && model.changeColumnName?uncap_first != "updateTime" && model.changeColumnName?uncap_first != "mark">
      <#if (model.columnType = 'INT UNSIGNED' || model.columnType = 'INT' || model.columnType = 'SMALLINT UNSIGNED' || model.columnType = 'SMALLINT' || model.columnType = 'BIGINT UNSIGNED' || model.columnType = 'BIGINT' || model.columnType = 'MEDIUMINT UNSIGNED' || model.columnType = 'MEDIUMINT'  || model.columnType = 'TINYINT UNSIGNED' || model.columnType = 'TINYINT')>
          <#if model.hasColumnCommentValue = true>
          ${model.columnName}: [
            {required: true, message: '请选择${model.columnCommentName}', type: 'number', trigger: 'blur'}
          ],
          <#else>
          ${model.columnName}: [
            {required: true, message: '请选择${model.columnComment}', type: 'number', trigger: 'blur'}
          ],
          </#if>
      <#else>
          ${model.columnName}: [
            {required: true, message: '请输入${model.columnComment}', type: 'string', trigger: 'blur'}
          ],
      </#if>
    </#if>
  </#list>
</#if>
        },
        // 提交状态
        loading: false,
        // 是否是修改
        isUpdate: false,
    <#if model_column?exists>
      <#list model_column as model>
        <#if model.columnImage == true>
        // 上传目录
        updDir: '${entityName?lower_case}',
        </#if>
      </#list>
    </#if>
      };
    },
    watch: {
      data() {
        if (this.data) {
          this.form = Object.assign({}, this.data);
          this.isUpdate = true;
        } else {
          this.form = {};
          this.isUpdate = false;
        }
        if (this.$refs.form) {
          this.$refs.form.clearValidate();
        }
      }
    },
    methods: {
      /* 保存编辑 */
      save() {
        this.$refs.form.validate().then(() => {
          this.loading = true;
          this.$http[this.isUpdate ? 'put' : 'post'](this.form.id ? '/system/${entityName?lower_case}/edit' : '/system/${entityName?lower_case}/add', this.form).then(res => {
            this.loading = false;
            if (res.data.code === 0) {
              this.$message.success(res.data.msg);
              if (!this.isUpdate) {
                this.form = {};
              }
              this.updateVisible(false);
              this.$emit('done');
            } else {
              this.$message.error(res.data.msg);
            }
          }).catch(e => {
            this.loading = false;
            this.$message.error(e.message);
          });
        }).catch(() => {
        });
      },
      /* 更新visible */
      updateVisible(value) {
        this.$emit('update:visible', value);
      }
    }
  }
</script>

<style scoped>
</style>