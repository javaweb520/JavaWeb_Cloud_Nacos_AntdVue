// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.framework.common.BaseQuery;
import com.javaweb.common.framework.utils.DateUtils;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.framework.utils.StringUtils;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.common.security.utils.SecurityUtils;
import com.javaweb.service.system.config.CommonConfig;
import com.javaweb.service.system.constant.ArticleConstant;
import com.javaweb.service.system.entity.Article;
import com.javaweb.service.system.mapper.ArticleMapper;
import com.javaweb.service.system.query.ArticleQuery;
import com.javaweb.service.system.service.IArticleService;
import com.javaweb.service.system.service.IItemCateService;
import com.javaweb.service.system.utils.CommonUtils;
import com.javaweb.service.system.vo.article.ArticleInfoVo;
import com.javaweb.service.system.vo.article.ArticleListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 文章管理表 服务类实现
 * </p>
 *
 * @author 鲲鹏
 * @since 2021-10-14
 */
@Service
public class ArticleServiceImpl extends BaseServiceImpl<ArticleMapper, Article> implements IArticleService {

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private IItemCateService itemCateService;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        ArticleQuery articleQuery = (ArticleQuery) query;
        // 查询条件
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        // 文章标题
        if (!StringUtils.isEmpty(articleQuery.getTitle())) {
            queryWrapper.like("title", articleQuery.getTitle());
        }
        // 状态：1正常 2停用
        if (!StringUtils.isEmpty(articleQuery.getStatus())) {
            queryWrapper.eq("status", articleQuery.getStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 获取数据列表
        IPage<Article> page = new Page<>(articleQuery.getPage(), articleQuery.getLimit());
        IPage<Article> pageData = articleMapper.selectPage(page, queryWrapper);
        pageData.convert(x -> {
            ArticleListVo articleListVo = Convert.convert(ArticleListVo.class, x);
            // 文章封面地址
            if (!StringUtils.isEmpty(articleListVo.getCover())) {
                articleListVo.setCover(CommonUtils.getImageURL(articleListVo.getCover()));
            }
            // 状态描述
            if (articleListVo.getStatus() != null && articleListVo.getStatus() > 0) {
                articleListVo.setStatusName(ArticleConstant.ARTICLE_STATUS_LIST.get(articleListVo.getStatus()));
            }
            // 所属栏目
            if (x.getCateId() > 0) {
                String cateName = itemCateService.getCateName(x.getCateId(), ">>");
                articleListVo.setCateName(cateName);
            }
            // 图集
            if (StringUtils.isNotEmpty(x.getImgs())) {
                List<String> stringList = new ArrayList<>();
                String[] strings = x.getImgs().split(",");
                for (String string : strings) {
                    stringList.add(CommonUtils.getImageURL(string));
                }
                articleListVo.setImgsList(stringList.toArray(new String[stringList.size()]));
            }
            // 富文本图片
            List<String> stringList = CommonUtils.getImgStr(x.getContent());
            if (stringList.size() > 0) {
                stringList.forEach(item -> {
                    articleListVo.setContent(x.getContent().replaceAll(item, CommonUtils.getImageURL(item)));
                });
            }
            return articleListVo;
        });
        return JsonResult.success(pageData);
    }

    /**
     * 获取详情Vo
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public Object getInfo(Serializable id) {
        Article entity = (Article) super.getInfo(id);
        // 返回视图Vo
        ArticleInfoVo articleInfoVo = new ArticleInfoVo();
        // 拷贝属性
        BeanUtils.copyProperties(entity, articleInfoVo);
        // 文章封面
        if (!StringUtils.isEmpty(articleInfoVo.getCover())) {
            articleInfoVo.setCover(CommonUtils.getImageURL(articleInfoVo.getCover()));
        }
        // 图集
        if (StringUtils.isNotEmpty(entity.getImgs())) {
            List<String> stringList = new ArrayList<>();
            String[] strings = entity.getImgs().split(",");
            for (String string : strings) {
                stringList.add(CommonUtils.getImageURL(string));
            }
            articleInfoVo.setImgsList(stringList.toArray(new String[stringList.size()]));
        }
        // 富文本图片
        List<String> stringList = CommonUtils.getImgStr(entity.getContent());
        if (stringList.size() > 0) {
            stringList.forEach(item -> {
                articleInfoVo.setContent(entity.getContent().replaceAll(item, CommonUtils.getImageURL(item)));
            });
        }
        return articleInfoVo;
    }

    /**
     * 添加、更新记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Article entity) {
        // 文章封面
        if (entity.getCover().contains(CommonConfig.imageURL)) {
            entity.setCover(entity.getCover().replaceAll(CommonConfig.imageURL, ""));
        }
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(SecurityUtils.getUserId());
            entity.setUpdateTime(DateUtils.now());
        } else {
            entity.setCreateUser(SecurityUtils.getUserId());
            entity.setCreateTime(DateUtils.now());
        }
        // 处理图集
        if (StringUtils.isNotNull(entity.getImgsList()) && entity.getImgsList().length > 0) {
            List<String> stringList = new ArrayList<>();
            for (int i = 0; i < entity.getImgsList().length; i++) {
                stringList.add(entity.getImgsList()[i].replace(CommonConfig.imageURL, ""));
            }
            entity.setImgs(String.join(",", stringList));
        }
        // 处理富文本
        entity.setContent(entity.getContent().replaceAll(CommonConfig.imageURL, ""));
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult delete(Article entity) {
        entity.setUpdateUser(1);
        entity.setUpdateTime(DateUtils.now());
        entity.setMark(0);
        return super.delete(entity);
    }

    /**
     * 设置状态
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult setStatus(Article entity) {
        if (entity.getId() == null || entity.getId() <= 0) {
            return JsonResult.error("记录ID不能为空");
        }
        if (entity.getStatus() == null) {
            return JsonResult.error("记录状态不能为空");
        }
        return super.setStatus(entity);
    }

}