// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.javaweb.common.framework.common.BaseQuery;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.framework.utils.StringUtils;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.service.system.config.CommonConfig;
import com.javaweb.service.system.entity.Item;
import com.javaweb.service.system.entity.ItemCate;
import com.javaweb.service.system.mapper.ItemCateMapper;
import com.javaweb.service.system.mapper.ItemMapper;
import com.javaweb.service.system.query.ItemCateQuery;
import com.javaweb.service.system.service.IItemCateService;
import com.javaweb.service.system.utils.CommonUtils;
import com.javaweb.service.system.vo.itemcate.ItemCateListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 栏目管理表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-07
 */
@Service
public class ItemCateServiceImpl extends BaseServiceImpl<ItemCateMapper, ItemCate> implements IItemCateService {

    @Autowired
    private ItemCateMapper itemCateMapper;
    @Autowired
    private ItemMapper itemMapper;

    /**
     * 获取栏目列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        ItemCateQuery itemCateQuery = (ItemCateQuery) query;
        // 查询条件
        QueryWrapper<ItemCate> queryWrapper = new QueryWrapper<>();
        // 栏目名称
        if (!StringUtils.isEmpty(itemCateQuery.getName())) {
            queryWrapper.like("name", itemCateQuery.getName());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        List<ItemCate> pageData = itemCateMapper.selectList(queryWrapper);
        List<ItemCateListVo> itemCateListVoList = new ArrayList<>();
        pageData.forEach(x -> {
            ItemCateListVo itemCateListVo = new ItemCateListVo();
            BeanUtils.copyProperties(x, itemCateListVo);
            // 封面
            itemCateListVo.setCover(CommonUtils.getImageURL(x.getCover()));
            // 所属站点
            if (StringUtils.isNotNull(x.getItemId())) {
                Item item = itemMapper.selectById(x.getItemId());
                if (item != null) {
                    itemCateListVo.setItemName(item.getName());
                }
            }
            itemCateListVoList.add(itemCateListVo);
        });
        return JsonResult.success(itemCateListVoList);
    }

    /**
     * 添加或编辑栏目
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(ItemCate entity) {
        // 图片处理
        if (!StringUtils.isEmpty(entity.getCover()) && entity.getCover().contains(CommonConfig.imageURL)) {
            entity.setCover(entity.getCover().replaceAll(CommonConfig.imageURL, ""));
        }
        return super.edit(entity);
    }

    /**
     * 获取栏目列表
     *
     * @return
     */
    @Override
    public JsonResult getCateList() {
        List<ItemCate> itemCateList = list(new LambdaQueryWrapper<ItemCate>()
                .eq(ItemCate::getStatus, 1)
                .eq(ItemCate::getMark, 1));
        if (!itemCateList.isEmpty()) {
            // 获取站点列表
            List<Item> itemList = itemMapper.selectList(new LambdaQueryWrapper<Item>()
                    .eq(Item::getStatus, 1)
                    .eq(Item::getMark, 1));
            Map<Integer, String> itemMap = itemList.stream().collect(Collectors.toMap(Item::getId, Item::getName));
            itemCateList.forEach(item -> {
                item.setName(String.format("【%s】%s", itemMap.get(item.getItemId()), item.getName()));
            });
        }
        return JsonResult.success(itemCateList);
    }

    /**
     * 获取栏目名称
     *
     * @param cateId    栏目ID
     * @param delimiter 分隔符
     * @return
     */
    @Override
    public String getCateName(Integer cateId, String delimiter) {
        List<String> nameList = new ArrayList<>();
        while (cateId > 0) {
            ItemCate cateInfo = itemCateMapper.selectById(cateId);
            if (cateInfo != null) {
                nameList.add(cateInfo.getName());
                cateId = cateInfo.getPid();
            } else {
                cateId = 0;
            }
        }
        // 使用集合工具实现数组翻转
        Collections.reverse(nameList);
        return org.apache.commons.lang3.StringUtils.join(nameList.toArray(), delimiter);
    }

}
