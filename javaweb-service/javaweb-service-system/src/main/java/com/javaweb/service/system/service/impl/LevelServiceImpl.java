// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.service.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.framework.common.BaseQuery;
import com.javaweb.common.framework.utils.DateUtils;
import com.javaweb.common.framework.utils.ExcelUtil;
import com.javaweb.common.framework.utils.JsonResult;
import com.javaweb.common.framework.utils.StringUtils;
import com.javaweb.common.security.common.BaseServiceImpl;
import com.javaweb.common.security.utils.SecurityUtils;
import com.javaweb.service.system.config.UploadConfig;
import com.javaweb.service.system.entity.Level;
import com.javaweb.service.system.mapper.LevelMapper;
import com.javaweb.service.system.query.LevelQuery;
import com.javaweb.service.system.service.ILevelService;
import com.javaweb.service.system.utils.UploadUtils;
import com.javaweb.service.system.vo.level.LevelInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 职级表 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-11-02
 */
@Service
public class LevelServiceImpl extends BaseServiceImpl<LevelMapper, Level> implements ILevelService {

    @Autowired
    private LevelMapper levelMapper;

    /**
     * 获取职级列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        LevelQuery levelQuery = (LevelQuery) query;
        // 查询条件
        QueryWrapper<Level> queryWrapper = new QueryWrapper<>();
        // 职级名称
        if (!StringUtils.isEmpty(levelQuery.getName())) {
            queryWrapper.like("name", levelQuery.getName());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        IPage<Level> page = new Page<>(levelQuery.getPage(), levelQuery.getLimit());
        IPage<Level> pageData = levelMapper.selectPage(page, queryWrapper);
        return JsonResult.success(pageData);
    }

    /**
     * 获取职级列表
     *
     * @return
     */
    @Override
    public JsonResult getLevelList() {
        QueryWrapper<Level> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", 1);
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");
        List<Level> levelList = list(queryWrapper);
        return JsonResult.success(levelList);
    }

    /**
     * 导入Excel数据
     *
     * @param request 网络请求
     * @param name    目录名称
     * @return
     */
    @Override
    public JsonResult importExcel(HttpServletRequest request, String name) {
        // 上传文件
        UploadUtils uploadUtils = new UploadUtils();
        uploadUtils.setDirName("files");
        Map<String, Object> result = uploadUtils.uploadFile(request, name);
        List<String> imageList = (List<String>) result.get("image");

        // 文件路径
        String filePath = UploadConfig.uploadFolder + imageList.get(imageList.size() - 1);
        // 读取文件
        List<Object> rows = ExcelUtil.readMoreThan1000RowBySheet(filePath, null);
        if (CollectionUtils.isEmpty(rows)) {
            return JsonResult.error("文件读取失败");
        }
        int totalNum = 0;
        for (int i = 1; i < rows.size(); i++) {
            // 排除第一行
            String info = rows.get(i).toString();
            if (info.length() <= 2) {
                continue;
            }

            info = info.substring(1, info.length() - 1);
            String[] cloumns = info.split(",\\s+");
            if (cloumns.length != 3) {
                continue;
            }

            // 插入数据
            Level level = new Level();
            level.setName(cloumns[0]);
            level.setStatus(cloumns[1].equals("正常") ? 1 : 2);
            level.setSort(StringUtils.isNull(cloumns[2]) ? 0 : Integer.valueOf(cloumns[2]));
            level.setCreateUser(SecurityUtils.getUserId());
            level.setCreateTime(DateUtils.now());
            int count = levelMapper.insert(level);
            if (count == 1) {
                totalNum++;
            }
        }
        return JsonResult.success(null, String.format("本次共导入数据【%s】条", totalNum));
    }

    /**
     * 导出Excel数据
     *
     * @param levelQuery 查询条件
     * @return
     */
    @Override
    public List<LevelInfoVo> exportExcel(LevelQuery levelQuery) {
        // 查询条件
        QueryWrapper<Level> queryWrapper = new QueryWrapper<>();
        // 职级名称
        if (!StringUtils.isEmpty(levelQuery.getName())) {
            queryWrapper.like("name", levelQuery.getName());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByAsc("sort");

        // 查询分页数据
        List<Level> levelList = levelMapper.selectList(queryWrapper);
        List<LevelInfoVo> levelInfoVoList = new ArrayList<>();
        if (!levelList.isEmpty()) {
            levelList.forEach(item -> {
                LevelInfoVo levelInfoVo = new LevelInfoVo();
                BeanUtils.copyProperties(item, levelInfoVo);
                levelInfoVoList.add(levelInfoVo);
            });
        }
        return levelInfoVoList;
    }
}
